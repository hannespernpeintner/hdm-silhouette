# README #

Bound is a story-driven 2D Jump-and-Run I developed with four fellow students of mine during my bachelor studies (three programmers, two designers). It features a rich component based game engine based on XNA 4.0 and integrates tons of things, for example 2D physics, audio and a rich game editor.

![bound_ingame.png](https://bitbucket.org/repo/jyeLeL/images/4068089729-bound_ingame.png)
![bound_editor.PNG](https://bitbucket.org/repo/jyeLeL/images/221371250-bound_editor.PNG)